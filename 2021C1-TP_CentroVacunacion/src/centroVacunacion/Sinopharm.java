package centroVacunacion;

public class Sinopharm extends VacunaATP {
	public Sinopharm(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso, 3);
	}

	@Override
	public boolean estaVencida() {
		return false;
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
