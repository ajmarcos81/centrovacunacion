package centroVacunacion;

public class Sputnik extends VacunaMayor60 {

	public Sputnik(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso, 3);

	}

	@Override
	public boolean estaVencida() {
		return false;
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
