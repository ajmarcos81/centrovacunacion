package centroVacunacion;

public class Persona {
	private int dni;
	private int edad;
	private boolean trabajadorSalud;
	private boolean enfermedadPreExistente;
	private String estado;

	public Persona(int dni, int edad, boolean enfermedadPreExistente, boolean trabajadorSalud) {
		if(dni<1000000) {
			throw new RuntimeException("El dni debe ser valido");
		}if(edad<18) {
			throw new RuntimeException("La edad debe ser mayor a 18");
		}
		this.dni = dni;
		this.edad = edad;
		this.trabajadorSalud = trabajadorSalud;
		this.enfermedadPreExistente = enfermedadPreExistente;
		this.estado = "espera";
	}

	public Persona(int dni) {//constructor numero dos 
		this.dni = dni;
	}

	public int devolverDni() {
		return dni;
	}

	public String devolverEstado() {
		return estado;
	}

	public void cambiarEstado(String est) {
		if(est=="espera" || est=="conTurno" || est=="vacunado") {//mas estados
			estado = est;
		}else {
			throw new RuntimeException("El estado debe ser vlido");
		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dni;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (dni != other.dni)
			return false;
		return true;
	}

	@Override
	public String toString() {

		StringBuilder cadena = new StringBuilder();
		cadena.append("Dni: ");
		cadena.append(dni);
		cadena.append("\n");
		cadena.append("Prioridad: ");
		cadena.append(damePrioridad());
		cadena.append("\n");
		cadena.append("Edad: ");
		cadena.append(edad);
		cadena.append("\n");
		if (trabajadorSalud == true) {
			cadena.append("Es trabajadorSalud");

		} else if (trabajadorSalud == false) {
			cadena.append("No Es trabajadorSalud");

		}
		cadena.append("\n");
		if (enfermedadPreExistente == true) {
			cadena.append("Tiene enfermedades PreExistentes ");
		} else {
			cadena.append("No tiene enfermedades PreExistentes ");
		}
		cadena.append("\n");
		cadena.append("Estado: ");
		cadena.append(estado);
		return cadena.toString();
	}

	public int damePrioridad() {
		if (trabajadorSalud == true) {
			return 1;

		} else if (edad >= 60) {
			return 2;
		} else if (enfermedadPreExistente == true) {
			return 3;
		} else {
			return 4;
		}
	}

}
