package centroVacunacion;


public class Turno {
	private Persona persona;
	private Fecha fecha;
	private Vacuna vacuna;
	private boolean cumplido;

	public Turno(Persona persona, Fecha fecha, Vacuna vacuna) {
		if(persona==null || fecha==null || vacuna ==null ) {
			throw new RuntimeException("Los parametros pasados por parametro deben ser diferentes de null");
		}
		this.persona = persona;
		this.fecha = fecha;
		this.vacuna = vacuna;
		this.cumplido = false;
	}

	public boolean estaCumplido() {
		return cumplido;
	}

	public void cumplir() {
		cumplido = true;
	}

	public Vacuna devolverVacuna() {
		return vacuna;
	}

	@Override
	public String toString() {
		StringBuilder cadena = new StringBuilder();
		cadena.append("--------------Turno-----------------");
		cadena.append("\n");
		cadena.append("Inscripto: ");
		cadena.append(persona.toString());
		cadena.append("\n");
		cadena.append("Fecha");
		cadena.append(fecha.toString());
		cadena.append("\n");
		cadena.append("Vacuna: ");
		cadena.append(vacuna.toString());
		cadena.append("\n");
		cadena.append("Estado de turno: ");
		if (cumplido == true) {
			cadena.append("Cumplido");
		} else {
			cadena.append("No cumplido");
		}
		cadena.append("\n");
		return cadena.toString();
	}

	public int devolverDniPersona() {
		return persona.devolverDni();
	}

	public Fecha devolverFecha() {
		return fecha;
	}

	public Persona devolverPersona() {
		return persona;
	}

}
