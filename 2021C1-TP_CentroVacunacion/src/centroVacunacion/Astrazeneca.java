package centroVacunacion;

public class Astrazeneca extends VacunaATP {

	public Astrazeneca(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso, 3);

	}

	@Override
	public boolean estaVencida() {
		return false;
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
