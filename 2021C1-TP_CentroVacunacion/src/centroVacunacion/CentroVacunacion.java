package centroVacunacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CentroVacunacion {

	private HashMap<Vacuna, Integer> deposito;
	private HashMap<Vacuna, Integer> reservadas;
	private HashMap<String, Integer> vencidas;
	private HashSet<Persona> listaDePersonas;
	private int capacidadVacunacion;
	private HashSet<Turno> turnos;
	private String nombre;

	/**
	 * Constructor. recibe el nombre del centro y la capacidad de vacunación diaria.
	 * Si la capacidad de vacunación no es positiva se debe generar una excepción.
	 * Si el nombre no está definido, se debe generar una excepción.
	 */

	public CentroVacunacion(String nombreCentro, int capacidadVacunacionDiaria) {
		if (capacidadVacunacionDiaria < 0) {
			throw new RuntimeException("La capacidad de vacunacion debe ser positiva");
		}
		if (nombreCentro.equals(null)) {
			throw new RuntimeException("El nombre del centro debe ser valido");
		}
		if (nombreCentro.isEmpty()) {
			throw new RuntimeException("El nombre del centro debe ser valido");
		} else {
			nombre = nombreCentro;
			capacidadVacunacion = capacidadVacunacionDiaria;
			deposito = new HashMap<>();
			listaDePersonas = new HashSet<>();
			reservadas = new HashMap<>();
			vencidas = new HashMap<>();
			turnos = new HashSet<>();

		}
	}

	/**
	 * Solo se pueden ingresar los tipos de vacunas planteados en la 1ra parte. Si
	 * el nombre de la vacuna no coincidiera con los especificados se debe generar
	 * una excepción. También se genera excepción si la cantidad es negativa. La
	 * cantidad se debe sumar al stock existente, tomando en cuenta las vacunas ya
	 * utilizadas.
	 */

	public void ingresarVacunas(String nombreVacuna, int cantidad, Fecha fechaIngreso) {

		if (cantidad <= 0) {
			throw new RuntimeException("La cantidad debe ser valida");
		} else {

			Vacuna vacuna = devolverVacuna(nombreVacuna, fechaIngreso);

			int n = deposito.getOrDefault(vacuna, 0);

			deposito.put(vacuna, n + cantidad);

		}
	}

	private Vacuna devolverVacuna(String nombre, Fecha fechaIngreso) {
		if (nombre.equals("Sputnik") || nombre.equals("sputnik")) {
			return new Sputnik(nombre, fechaIngreso);
		} else if (nombre.equals("AstraZeneca") || nombre.equals("astraZeneca") || nombre.equals("astrazeneca")) {
			return new Astrazeneca(nombre, fechaIngreso);
		} else if (nombre.equals("Pfizer") || nombre.equals("pfizer") ) {
			return new Pfizer(nombre, fechaIngreso);
		} else if (nombre.equals("Sinopharm") || nombre.equals("sinopharm")) {
			return new Sinopharm(nombre, fechaIngreso);
		}
		return new Moderna(nombre, fechaIngreso);

	}

	/**
	 * total de vacunas disponibles no vencidas sin distinción por tipo.
	 */

	public int vacunasDisponibles() {
		int total = 0;

		for (Vacuna v : deposito.keySet()) {

			if (!v.estaVencida()) {
				total = total + deposito.get(v);
			}

		}
		return total;
	}

	/**
	 * total de vacunas disponibles no vencidas que coincida con el nombre de vacuna
	 * especificado.
	 */
	public int vacunasDisponibles(String nombreVacuna) {
		int total = 0;

		for (Vacuna v : deposito.keySet()) {
			if (v.devolverNombre().equals(nombreVacuna) && !v.estaVencida()) {

				total = total + deposito.get(v);
			}
		}
		return total;
	}

	/**
	 * Se inscribe una persona en lista de espera. Si la persona ya se encuentra
	 * inscripta o es menor de 18 años, se debe generar una excepción. Si la persona
	 * ya fue vacunada, también debe generar una excepción.
	 */
	public void inscribirPersona(int dni, Fecha nacimiento, boolean tienePadecimientos, boolean esEmpleadoSalud) {

		if (menor18(nacimiento)) {
			throw new RuntimeException("La persona debe tener mas de 18 años,no se puede inscribir");
		}
		if (estaInscripta(dni)) {
			throw new RuntimeException("La persona ya esta inscripta,no se puede inscribir nuevamente");
		}
		if (fueVacunada(dni)) {
			throw new RuntimeException("La persona ya fue vacunada ,no se puede inscribir nuevamente");

		} else {
			int edad = dameEdad(nacimiento);
			Persona persona = new Persona(dni, edad, tienePadecimientos, esEmpleadoSalud);
			listaDePersonas.add(persona);
		}

	}

	private int dameEdad(Fecha nacimiento) {
		return Fecha.diferenciaAnios(Fecha.hoy(), nacimiento);
	}

	private boolean menor18(Fecha nacimiento) {
		return Fecha.diferenciaAnios(Fecha.hoy(), nacimiento) < 18;
	}

	private boolean estaInscripta(int dni) {
		for (Persona p : listaDePersonas) {
			if (esMismaPersona(dni, p)) {
				return true;
			}
		}
		return false;
	}

	private boolean fueVacunada(int dni) {
		for (Persona p : listaDePersonas) {
			if (esMismaPersona(dni, p)) {
				if (p.devolverEstado().equals("vacunada"))
					;
				return true;
			}
		}
		return false;
	}

	private boolean esMismaPersona(int dni, Persona p) {
		return p.devolverDni()==dni;
	}

	/**
	 * Devuelve una lista con los DNI de todos los inscriptos que no se vacunaron y
	 * que no tienen turno asignado. Si no quedan inscriptos sin vacunarse debe
	 * devolver una lista vacía.
	 */

	public List<Integer> listaDeEspera() {
		List<Integer> lista = new ArrayList<>();
		for (Persona p : listaDePersonas)
			if (p.devolverEstado().equals("espera")) {
				lista.add(p.devolverDni());
			}
		return lista;
	}

	/**
	 * Primero se verifica si hay turnos vencidos. En caso de haber turnos vencidos,
	 * la persona que no asistió al turno debe ser borrada del sistema y la vacuna
	 * reservada debe volver a estar disponible.
	 *
	 * Segundo, se deben verificar si hay vacunas vencidas y quitarlas del sistema.
	 *
	 * Por último, se procede a asignar los turnos a partir de la fecha inicial
	 * recibida según lo especificado en la 1ra parte. Cada vez que se registra un
	 * nuevo turno, la vacuna destinada a esa persona dejará de estar disponible.
	 * Dado que estará reservada para ser aplicada el día del turno.
	 *
	 *
	 */
	public void generarTurnos(Fecha fechaInicial) {
		turnosVencidos();
		eliminarVacunasVencidas();
		
		Fecha fecha = new Fecha(fechaInicial);

		if (fecha.anterior(Fecha.hoy())) {
			throw new RuntimeException("la fecha debe ser posterior a la actual");
		} else {

			int capacidad = capacidadVacunacion;
			
			for (int prioridad = 1; prioridad <= 4; prioridad++) {
				ArrayList<Persona> personas = dameListaConMayorPrioridad(prioridad);
				if (personas != null && personas.size() > 0) {
				
					for (int pos = 0; pos < personas.size(); pos++) {
						if (capacidad == 0) {
							fecha.avanzarUnDia();
							capacidad = capacidadVacunacion;
						}
						Vacuna v = pedirVacuna(prioridad);
						if (v != null) {
							
							asignarTurno(v, personas.get(pos),fecha);					
							capacidad--;
							
						} else {
							break;
						}
				}
			}

			}
		}
	}


	private void asignarTurno(Vacuna vacuna, Persona persona, Fecha fecha) {
		
		persona.cambiarEstado("conTurno");
		turnos.add(new Turno(persona, new Fecha(fecha), vacuna));
		
	}

	private ArrayList<Persona> dameListaConMayorPrioridad(int prioridad) {
		ArrayList<Persona> personas = new ArrayList<>();
		for (Persona p : listaDePersonas) {
			if (p.damePrioridad() == prioridad) {
				personas.add(p);
			}
		}
		return personas;
	}

	private Vacuna pedirVacuna(int prioridad) {
		for (Vacuna v : deposito.keySet()) {
			if (prioridad == 2) {
				quitarUnaVacunaDeDeposito(v);
				agregarReservadas(v);
				return v;
				
			} else if (!(v instanceof VacunaMayor60)){
				quitarUnaVacunaDeDeposito(v);
				agregarReservadas(v);
				return v;
			}
		}
		return null;
	}

	private void agregarReservadas(Vacuna v) {
		int reservada = reservadas.getOrDefault(v, 0);
		reservadas.put(v, reservada + 1);

	}

	private void quitarVacunaReservada(Vacuna v) {
		int reservada = reservadas.getOrDefault(v, 0);
		reservadas.put(v, reservada - 1);
	}

	private void turnosVencidos() {

		Iterator<Turno> iterator = turnos.iterator();

		while (iterator.hasNext()) {

			Turno turno = iterator.next();

			if (turno.devolverFecha().anterior(Fecha.hoy())) {
				cargarVacunaNuevamente(turno.devolverVacuna());
				quitarVacunaReservada(turno.devolverVacuna());
				eliminarPersona(turno.devolverPersona());

				iterator.remove();

			}
		}

	}

	public void eliminarVacunasVencidas() {

		Iterator<Vacuna> iterator = deposito.keySet().iterator();
		while (iterator.hasNext()) {
			Vacuna vacuna = iterator.next();
			if (vacuna.estaVencida()) {
				agregarCajaVacunasVencidas(vacuna);
				iterator.remove();
			}
		}
	}

	private void cargarVacunaNuevamente(Vacuna vacuna) {
		int cantidad = deposito.getOrDefault(vacuna, 0);
		deposito.put(vacuna, cantidad + 1);
	}

	private void agregarCajaVacunasVencidas(Vacuna cVacunas) {
		vencidas.put(cVacunas.devolverNombre(), devolverCantidadVacunas(cVacunas));
	}

	private int devolverCantidadVacunas(Vacuna cVacunas) {
		return deposito.get(cVacunas);
	}

	private void eliminarPersona(Persona persona) {
		listaDePersonas.remove(persona);
	}

	/**
	 * Devuelve una lista con los dni de las personas que tienen turno asignado para
	 * la fecha pasada por parámetro. Si no hay turnos asignados para ese día, se
	 * debe devolver una lista vacía. La cantidad de turnos no puede exceder la
	 * capacidad por día de la ungs.
	 */

	public List<Integer> turnosConFecha(Fecha fecha) {
		List<Integer> lista = new ArrayList<>();

		for (Turno t : turnos) {

			if (t.devolverFecha().compareTo(fecha) == 0) {

				lista.add(t.devolverDniPersona());
			}
		}

		return lista;
	}

	/**
	 * Dado el DNI de la persona y la fecha de vacunación se valida que esté
	 * inscripto y que tenga turno para ese dia. - Si tiene turno y está inscripto
	 * se debe registrar la persona como vacunada y la vacuna se quita del depósito.
	 * - Si no está inscripto o no tiene turno ese día, se genera una Excepcion.
	 */

	public void vacunarInscripto(int dni, Fecha fechaVacunacion) {

		if (!inscripto(dni)) {
			throw new RuntimeException("La persona no esta inscripta");
		} else if (!conTurno(dni, fechaVacunacion)) {
			throw new RuntimeException("La persona no tiene turno");

		} else {
			Persona p = devolverPersona(dni);
			p.cambiarEstado("vacunado");
			Turno t = devolverTurno(dni, fechaVacunacion);
			t.cumplir();
			quitarUnaVacunaDeDeposito(t.devolverVacuna());
		}
	}

	private void quitarUnaVacunaDeDeposito(Vacuna vacuna) {

		int cantidad = deposito.getOrDefault(vacuna, 0);

		deposito.put(vacuna, cantidad - 1);

	}

	private boolean inscripto(int dni) {
		return listaDePersonas.contains(devolverPersona(dni));
	}

	private Persona devolverPersona(int dni) {
		Persona p = new Persona(dni);
		for (Persona persona : listaDePersonas) {
			if (persona.equals(p)) {
				return persona;
			}
		}
		return null;
	}

	private Turno devolverTurno(int dni, Fecha fVacunacion) {
		for (Turno t : turnos) {
			if (t.devolverFecha().compareTo(fVacunacion) == 0 && t.devolverDniPersona() == dni) {
				return t;
			}
		}
		return null;
	}

	private boolean conTurno(int dni, Fecha fVacunacion) {
		for (Turno t : turnos) {
			if (t.devolverFecha().compareTo(fVacunacion) == 0 && t.devolverDniPersona() == dni) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Devuelve un Diccionario donde - la clave es el dni de las personas vacunadas
	 * - Y, el valor es el nombre de la vacuna aplicada.
	 */
	public Map<Integer, String> reporteVacunacion() {
		HashMap<Integer, String> reporte = new HashMap<>();
		for (Persona p : listaDePersonas) {
			if (p.devolverEstado().equals("vacunado")) {
				String n = dameNombreVacuna(p);
				reporte.put(p.devolverDni(), n);
			}
		}
		return reporte;
	}

	private String dameNombreVacuna(Persona p) {
		for (Turno t : turnos) {
			if (t.devolverPersona().equals(p)) {
				return t.devolverVacuna().devolverNombre();
			}
		}
		return "";
	}

	/**
	 * Devuelve en O(1) un Diccionario: - clave: nombre de la vacuna - valor:
	 * cantidad de vacunas vencidas conocidas hasta el momento.
	 */
	public Map<String, Integer> reporteVacunasVencidas() {
		return vencidas;
	}

	@Override
	public String toString() {
		StringBuilder cadena = new StringBuilder();
		cadena.append("Nombre Del Centro: ");
		cadena.append(nombre);
		cadena.append("\n");
		cadena.append("Capacidad de vacunacion: ");
		cadena.append(capacidadVacunacion);
		cadena.append("\n");
		cadena.append("VacunasDisponibles: ");
		cadena.append(vacunasDisponibles());
		cadena.append("\n");
		cadena.append("Cantidad de personas en lista de espera: ");
		cadena.append(cantidadDePersonasEnListaDeEspera());
		cadena.append("\n");
		cadena.append("Cantidad de turnos asignados: ");
		cadena.append(cantidadDeTurnosAsignados());
		cadena.append("\n");
		cadena.append("Cantidad de vacunas aplicadas hasta el momento: ");
		cadena.append(cantidadVacunasAplicadas());
		cadena.append("\n");
		return cadena.toString();
	}

	private int cantidadDePersonasEnListaDeEspera() {
		return listaDeEspera().size();
	}

	private int cantidadVacunasAplicadas() {
		return reporteVacunacion().size();
	}

	private int cantidadDeTurnosAsignados() {
		return turnos.size();
	}

}
