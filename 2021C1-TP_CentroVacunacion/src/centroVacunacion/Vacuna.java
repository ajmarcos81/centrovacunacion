package centroVacunacion;

public abstract class Vacuna {
	private String nombre;
	private Fecha fechaIngreso;
	private int temperatura;

	public Vacuna(String nombre, Fecha fechaIngreso, int temperatura) {// revisar temp

		if (esValida(nombre) && fechaValida(fechaIngreso)) {

			this.nombre = nombre;
			this.fechaIngreso = fechaIngreso;
			this.temperatura = temperatura;

		} else {
			throw new RuntimeException("Los parametros ingresados en vacuna no son validos ");
		}

	}

	private boolean fechaValida(Fecha fechaIngreso) {
		if (fechaIngreso.compareTo(Fecha.hoy()) > 0) {
			throw new RuntimeException("La fecha de ingreso debe ser anterior a la fecha actual ");
		}
		return true;
	}

	public boolean esValida(String nombre) {

		if (nombre.equals("Sputnik") || nombre.equals("AstraZeneca") || nombre.equals("Moderna")
				|| nombre.equals("Pfizer") || nombre.equals("Sinopharm")) {
			return true;
		}
		return false;

	}

	public Fecha getFechaIngreso() {
		return fechaIngreso;
	}

	public int devolverTemperatura() {
		return temperatura;
	}

	public abstract boolean estaVencida();

	@Override
	public String toString() {
		StringBuilder cadena = new StringBuilder();
		cadena.append("Nombre: ");
		cadena.append(nombre);
		cadena.append("\n");
		cadena.append("Fecha de Ingreso: ");
		cadena.append(fechaIngreso);
		cadena.append("\n");
		cadena.append("Temperatura: ");
		cadena.append(temperatura);
		return cadena.toString();
	}

	public String devolverNombre() {
		return nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaIngreso == null) ? 0 : fechaIngreso.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vacuna other = (Vacuna) obj;
		if (fechaIngreso == null) {
			if (other.fechaIngreso != null)
				return false;
		} else if (!fechaIngreso.equals(other.fechaIngreso))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
