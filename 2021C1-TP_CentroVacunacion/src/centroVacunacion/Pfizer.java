package centroVacunacion;

public class Pfizer extends VacunaMayor60 {
	private int duracion;

	public Pfizer(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso, -18);
		duracion = 30;
	}

	
	public boolean estaVencida() {
		Fecha ingreso = new Fecha(getFechaIngreso());
		ingreso.avanzarDias(duracion);
		return Fecha.hoy().compareTo(ingreso)>0;
	}

	@Override
	public String toString() {
		StringBuilder cadena = new StringBuilder();
		cadena.append(super.toString());
		cadena.append("Duracion: ");
		cadena.append(duracion);
		return cadena.toString();
	}

}
