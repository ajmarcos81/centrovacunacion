package centroVacunacion;

public class Moderna extends VacunaATP {
	private int duracion;

	public Moderna(String nombre, Fecha fechaIngreso) {
		super(nombre, fechaIngreso, -18);
		duracion = 60;
	}

	@Override
	public String toString() {
		StringBuilder cadena = new StringBuilder();
		cadena.append(super.toString());
		cadena.append("Duracion: ");
		cadena.append(duracion);
		return cadena.toString();

	}

	@Override
	public boolean estaVencida() {
		Fecha ingreso = new Fecha(getFechaIngreso());
		ingreso.avanzarDias(duracion);
		
		return Fecha.hoy().compareTo(ingreso)>0;

	}
	

}
